# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=libkomparediff2
pkgver=23.08.1
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/development"
pkgdesc="Library to compare files and strings"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcodecs5-dev
	kconfig5-dev
	kcoreaddons5-dev
	ki18n5-dev
	kio5-dev
	kxmlgui5-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/sdk/libkomparediff2.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkomparediff2-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7851aaa0110152cb8709fbe50e6a243d0363e1e8ae3093e01a01d2c6123a24cab17aad23cfba8b4f7e43ce1ac95ccb09e21f9669cbb346cb7198328f055a032a  libkomparediff2-23.08.1.tar.xz
"
