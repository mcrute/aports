# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=certbot
pkgver=2.7.0
pkgrel=0
pkgdesc="An ACME client that can update Apache/Nginx configurations"
url="https://github.com/certbot/certbot"
arch="noarch"
license="Apache-2.0"
depends="
	py3-acme
	py3-configargparse
	py3-configobj
	py3-cryptography
	py3-distro
	py3-distutils-extra
	py3-josepy
	py3-parsedatetime
	py3-pyrfc3339
	py3-setuptools
	py3-tz
	"
makedepends="
	py3-gpep517
	py3-wheel
	"
checkdepends="py3-augeas py3-pytest-xdist"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/certbot/certbot/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/certbot-$pkgver/$pkgname"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -n auto -p no:warnings
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
1e1defcd143f804a36b50e3104d239bf20873545474ceb654269a5879e15cfbb76a18bd569f5e6e12de1036779e72c74d9896707daabc314fc5d67acc1c5180d  certbot-2.7.0.tar.gz
"
