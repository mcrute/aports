# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=frameworkintegration5
pkgver=5.110.0
pkgrel=0
pkgdesc="Framework providing components to allow applications to integrate with a KDE Workspace"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org"
license="LGPL-2.1-or-later"
depends_dev="
	appstream-dev
	kconfig5-dev
	kconfigwidgets5-dev
	ki18n5-dev
	kiconthemes5-dev
	knewstuff5-dev
	knotifications5-dev
	kpackage5-dev
	kwidgetsaddons5-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/frameworkintegration.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/frameworkintegration-$pkgver.tar.xz"
subpackages="$pkgname-dev"
builddir="$srcdir/frameworkintegration-$pkgver"

replaces="frameworkintegration<=5.110.0"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4571485b1ff815b8cca2a12d19cd103210471647de4b5971fdd866fe08ec7e0f6b5b36c43dcb062bc2234d7c6901d279b25eac7a44e5188e0ee4fe9a646b79db  frameworkintegration-5.110.0.tar.xz
"
